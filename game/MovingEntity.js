'use strict';

class MovingEntity extends Entity {
	constructor(x, y, w, h, color, sprite, type) {
		super(x, y, w, h, color, sprite, type);
		this.velocity = [0, 0];
		this.slowdown = 1;
		this.speed = 5;
	}

	update() {
		if (this.velocity[0] != 0) {
			if (this.velocity[0] > 0) {
				if (this.velocity[0] > this.slowdown) {
					this.velocity[0] -= this.slowdown;
				} else {
					this.velocity[0] = 0;
				}
			} else {
				if (this.velocity[0] < -this.slowdown) {
					this.velocity[0] += this.slowdown;
				} else {
					this.velocity[0] = 0;
				}
			}
		}

		if (this.velocity[1] != 0) {
			if (this.velocity[1] > 0) {
				if (this.velocity[1] > this.slowdown) {
					this.velocity[1] -= this.slowdown;
				} else {
					this.velocity[1] = 0;
				}
			} else {
				if (this.velocity[1] < -this.slowdown) {
					this.velocity[1] += this.slowdown;
				} else {
					this.velocity[1] = 0;
				}
			}
		}

		this.pos.x += this.velocity[0];
		this.pos.y += this.velocity[1];
		if (Math.abs(this.velocity[1]) > 50) {
			this.velocity[1] = this.velocity[1] > 0 ? 50 : -50
		}
		super.update();
	}
}