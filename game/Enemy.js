'use strict';

class Enemy extends MovingEntity {
	constructor(x, y, w, h, color, sprite, type) {
		super(x, y, w, h, color, sprite, type);
		this.direction = -1;
		this.speed = 2;
	}

	update() {
		this.velocity[0] = this.direction * this.speed;
		this.velocity[1] += 1.6;
		super.update();
	}
}