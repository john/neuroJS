'use strict';

class Entity {
	constructor(x, y, w, h, color, sprite, type) {
		this.pos = new Point(x, y);
		this.width = w;
		this.height = h;
		this.color = color;
		this.sprite = sprite;
		this.type = type;
	}

	checkCollision(entity, uncollide) {
		var vX = (this.pos.x + (this.width / 2)) - (entity.pos.x + (entity.width / 2));
		var vY = (this.pos.y + (this.height / 2)) - (entity.pos.y + (entity.height / 2));
		var hWidths = (this.width / 2) + (entity.width / 2);
		var hHeights = (this.height / 2) + (entity.height / 2);
		var colDir = null;

		if (Math.abs(vX) < hWidths && Math.abs(vY) < hHeights) {
			// figures out on which side we are colliding (top, bottom, left, or right)
			var oX = hWidths - Math.abs(vX),
				oY = hHeights - Math.abs(vY);
			if (oX >= oY) {
				if (vY > 0) {
					if (uncollide) {
						this.pos.y += oY;
					}
					return 't';
				} else {
					if (uncollide) {
						this.pos.y -= oY;
					}
					return 'b';
				}
			} else {
				if (vX > 0) {
					if (uncollide) {
						this.pos.x += oX;
					}
					return 'l';
				} else {
					if (uncollide) {
						this.pos.x -= oX;
					}
					return 'r';
				}
			}
		}
		return colDir;
	}

	update() {
		return;
	}
}

function intersectLineRectangle(a1, a2, min, max) {

	var topRight = new Point( max.x, min.y );
	var bottomLeft = new Point( min.x, max.y );
	
	var inter1 = intersectLineLine(min, topRight, a1, a2);
	if (inter1) {
		return inter1;
	}
	var inter2 = intersectLineLine(topRight, max, a1, a2);
	if (inter2) {
		return inter2;
	}
	var inter3 = intersectLineLine(max, bottomLeft, a1, a2);
	if (inter3) {
		return inter3;
	}
	var inter4 = intersectLineLine(bottomLeft, min, a1, a2);
	if (inter4) {
		return inter4;
	}
	return false;
};

function intersectLineLine(a1, a2, b1, b2) {
	var ua_t = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x);
	var ub_t = (a2.x - a1.x) * (a1.y - b1.y) - (a2.y - a1.y) * (a1.x - b1.x);
	var u_b  = (b2.y - b1.y) * (a2.x - a1.x) - (b2.x - b1.x) * (a2.y - a1.y);

	if ( u_b != 0 ) {
		var ua = ua_t / u_b;
		var ub = ub_t / u_b;

		if ( 0 <= ua && ua <= 1 && 0 <= ub && ub <= 1 ) {
			return new Point(
				a1.x + ua * (a2.x - a1.x),
				a1.y + ua * (a2.y - a1.y)
			);
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function Point(x, y) {
	this.x = x;
	this.y = y;
}
