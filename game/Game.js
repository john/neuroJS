'use strict';
class Game {
	constructor(canvas) {
		this.canvas = document.getElementById(canvas);
		this.ctx = this.canvas.getContext('2d');

		this.speed = 1;
		this.last_frame_time_ms = 0;
		this.delta = 0;

		this.width = this.canvas.width;
		this.height = this.canvas.height;

		this.leveldata = leveldata_combined;
		this.level = new Level(this.leveldata);

		this.timeoutClock = 0;

		this.drawmode = 2;

		this.score = 0;

		this.loopCallback = function() {};
		this.drawCallback = function() {};
		this.resetCallback = function() {};

		this.hpLabel = document.createElement('label');
		this.scoreLabel = document.createElement('label');

		document.getElementById('body').appendChild(this.hpLabel);
		document.getElementById('body').appendChild(this.scoreLabel);

		var self = this;

		this.sprites = [];

		this.loadSprites();

		this.inputs = [
			false, false, false, false
		];

		document.onkeydown = function(e) {
			if (e.keyCode == '38' && !this.blockJump) {
				this.blockJump = true;
				self.inputs[0] = true;
			}
			else if (e.keyCode == '40') {
				self.inputs[1] = true;
			}
			else if (e.keyCode == '37') {
				self.inputs[2] = true;
			}
			else if (e.keyCode == '39') {
				self.inputs[3] = true;
			}
		};

		document.onkeyup= function(e) {
			if (e.keyCode == '38') {
				this.blockJump = false;
				self.inputs[0] = false;
			}
			else if (e.keyCode == '40') {
				self.inputs[1] = false;
			}
			else if (e.keyCode == '37') {
				self.inputs[2] = false;
			}
			else if (e.keyCode == '39') {
				self.inputs[3] = false;
			}
		};

		window.onresize = function () {
			self.setSize();
		};

		this.setSize();

	}

	loadSprites() {
		let sprites = [
			'block.png',
			'block_left.png',
			'block_right.png',
			'block_top.png',
			'block_right_left.png',
			'coin.png',
			'block_free.png'
		];

		for (var i = 0; i < sprites.length; i++) {

			var s = [];
			s[0] = new Image();
			s[0].name = sprites[i];
			s[1] = false;
			s[0].onload = function() {
				game.sprites[this.name][1] = true;
			};
			s[0].onerror = function() {
				console.log('err',this);
				//s[1] = true;
			};
			s[0].src = 'game/gfx/' + sprites[i];

			this.sprites[sprites[i]] = s;
		}
	}

	setSize() {
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
		this.scale = this.canvas.height / this.level.height;
		this.width = this.canvas.width;
		this.height = this.canvas.height;
		this.gradient = this.ctx.createLinearGradient(0,0,0,this.height);
		this.gradient.addColorStop(0,"#3ab7ff");
		this.gradient.addColorStop(1,"#c8ebff");
	}

	reset(init) {
		let org = this.speed;
		this.inputs = [
			false, false, false, false
		];
		this.blockJump = false;
		this.speed = 0;
		this.resetCallback(init);
		this.level.init(this.leveldata);
		this.player = new Player(20, 600, 25, 50, 'blue');
		this.level.addEntity(this.player, 'player');
		this.score = 0;
		this.jumps = 0;
		this.currentFrame = 0;
		this.speed = org;
		this.last_frame_time_ms = 0;
		this.delta = 0;
	}

	processInputs() {

		if (window.gamepad) {
			if (window.gamepad.buttons[11].pressed) {
				this.inputs[2] = true;
			} else {
				this.inputs[2] = false;
			}
			if (window.gamepad.buttons[12].pressed) {
				this.inputs[3] = true;
			} else {
				this.inputs[3] = false;
			}
			if (window.gamepad.buttons[14].pressed) {
				this.inputs[1] = true;
			} else {
				this.inputs[1] = false;
			}
			if (window.gamepad.buttons[0].pressed && !this.blockJump) {
				this.blockJump = true;
				this.inputs[0] = true;
			} else if (!window.gamepad.buttons[0].pressed) {
				this.blockJump = false;
				this.inputs[0] = false;
			}
		}

		if (this.inputs[0]) {
			this.player.jump();
			this.jumps++;
			this.inputs[0] = false;
		} 

		if (this.inputs[1]) {
			this.player.sprint();
			this.inputs[1] = false;
		} else {
			this.player.unsprint();
		}

		if (this.inputs[2]) {
			this.player.left();
			this.inputs[2] = false;
		}

		if (this.inputs[3]) {
			this.player.right();
			this.inputs[3] = false;
		}

	}

	checkCollisions() {
		for (let statics = 0; statics < this.level.collisionGroups.statics.length; statics++) {
			let col = this.player.checkCollision(this.level.collisionGroups.statics[statics], true);
			if (col) {
				if (col == 't' || col == 'b') {
					if (col == 'b' && this.player.velocity[1] < 1) {
						this.player.jumping = false;
					}
					this.player.velocity[1] = 0;
				} else if (col == 'r' || col == 'l') {
					this.player.velocity[0] = 0;
				}
			}
			for (let e = 0; e < this.level.collisionGroups.enemies.length; e++) {
				let col = this.level.collisionGroups.enemies[e].checkCollision(this.level.collisionGroups.statics[statics], true);
				if (col) {
					if (col == 't' || col == 'b') {
						this.level.collisionGroups.enemies[e].velocity[1] = 0;
					} else if (col == 'r' || col == 'l') {
						this.level.collisionGroups.enemies[e].direction *= -1;
					}
				}
			}
		}

		for (let enemy = 0; enemy < this.level.collisionGroups.enemies.length; enemy++) {
			let col = this.player.checkCollision(this.level.collisionGroups.enemies[enemy]);
			let above = (col && (this.player.pos.y + this.player.height / 2 < this.level.collisionGroups.enemies[enemy].pos.y));
			if (col == 'b' || above) {
				this.level.entities.splice(this.level.entities.indexOf(this.level.collisionGroups.enemies[enemy]), 1);
				this.level.collisionGroups.enemies.splice(enemy, 1);
				this.player.velocity[1] = -25;
				this.score += 100;
			} else if(col == 'r' || col == 'l' || col == 't') {
				this.player.hp -= 50;
			}
		}


		for (let coin = 0; coin < this.level.collisionGroups.coins.length; coin++) {
			let col = this.player.checkCollision(this.level.collisionGroups.coins[coin]);
			if (col) {
				this.level.entities.splice(this.level.entities.indexOf(this.level.collisionGroups.coins[coin]), 1);
				this.level.collisionGroups.coins.splice(coin, 1);
				this.score += 100;
			}
		}


		for (let killbox = 0; killbox < this.level.collisionGroups.killbox.length; killbox++) {
			let col = this.player.checkCollision(this.level.collisionGroups.killbox[killbox]);
			if (col) {
				this.player.hp -= 50;
			}
		}
	}

	addEntity(entity, collisionGroup) {
		this.level.collisionGroups[collisionGroup].push(entity);
		this.level.entities.push(entity);
	}

	mappedPosition(p) {
		return new Point(p.x - Math.max(this.player.pos.x - (this.width / 2), 0), p.y);
	}

	scalePosition(p) {
		return new Point(p.x * this.scale, p.y * this.scale);
	}

	draw() {
		if (options.DRAW_NETWORK) {
			this.network.draw();
		}

		if (this.drawmode > 0) {



			if (this.drawmode == 2) {
				this.ctx.fillStyle = this.gradient;
				this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
			} else {
				this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
			}



			for (let i = 0; i < this.level.entities.length; i++) {
				let e = this.level.entities[i];
				let p = this.scalePosition(this.mappedPosition(e.pos));
				if (p.x < this.width) {
					if (this.drawmode == 2 && e.sprite && this.sprites[e.sprite] && this.sprites[e.sprite][1]) {
						this.ctx.drawImage(this.sprites[e.sprite][0], Math.ceil(p.x), Math.ceil(p.y), Math.ceil(e.width * this.scale), Math.ceil(e.height * this.scale));
					} else {
						this.ctx.fillStyle = e.color;
						this.ctx.fillRect(p.x, p.y, Math.ceil(e.width * this.scale), Math.ceil(e.height * this.scale));	
					}
				}
			}
		}
	}

	update(timestamp) {
		let timestep = 16.66666;
		this.delta += timestamp - this.last_frame_time_ms;
		this.last_frame_time_ms = timestamp;

		if (this.delta > timestep * 30) {
			this.delta = timestep * 30; //limit delta to prevent glitches
		}
		this.draw();

		while (this.delta >= timestep) {
			for (let run = 0; run < this.speed; run++) {
				this.processInputs();
				for (let e = 0; e < this.level.entities.length; e++) {
					this.level.entities[e].update();
				}
				this.checkCollisions();

				if (this.player.hp <= 0) {
					this.reset();
					return;
				}
				this.currentFrame++;
				this.loopCallback();
			}
			this.delta -= timestep;
		}

		this.drawCallback();

	}
}

function radiantLine(centerX, centerY, innerRadius, outerRadius, degrees){

	let radians = degrees*Math.PI/180;
	let innerX = centerX + innerRadius * Math.cos(radians);
	let innerY = centerY + innerRadius * Math.sin(radians);
	let outerX = centerX + outerRadius * Math.cos(radians);
	let outerY = centerY + outerRadius * Math.sin(radians);
	return [new Point(innerX, innerY), new Point(outerX, outerY)];

}

function update(timestamp) {
	game.update(timestamp || 0);
	window.requestAnimationFrame(update);
}

