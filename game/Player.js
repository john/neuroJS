'use strict';

class Player extends MovingEntity {
	constructor(x, y, w, h, color, sprite, type) {
		super(x, y, w, h, color, sprite, type);
		this.initialHeight = h;
		this.hp = 50;
		this.jumping = false;
		this.speed = 7;
	}

	jump() {
		if (!this.jumping) {
			this.jumping = true;
			this.velocity[1] -= 27; 
		}
	}

	unsprint() {
		this.speed = 7;
	}

	sprint() {
		if (!this.jumping) {
			this.speed = 10;
		}
	}

	left() {
		this.velocity[0] = -this.speed;
	}

	right() {
		this.velocity[0] = this.speed;
	}

	update() {
		this.velocity[1] += 1.8;
		super.update();
	}
}