'use strict';


class Innovation {
	constructor() {
		this.innovations = [];
	}

	has(innovation) {
		if(innovation.length > 5) {
			console.error('bogus innovation');
		}
		for (var i = 0; i < this.innovations.length; i++) {
			if (
				this.innovations[i][0] === innovation[0]
				&& this.innovations[i][1] === innovation[1]
				&& this.innovations[i][2] === innovation[2]
				&& this.innovations[i][3] === innovation[3]
				&& this.innovations[i][4] === innovation[4]
			) {
				return i;
			}
		}
		return -1;
	}

	add(innovation) {
		let found = this.has(innovation);
		if (found != -1) {
			return found;
		} else {
			this.innovations.push(innovation);
			return this.innovations.length - 1;
		}
	}
}

var innovationTracker = new Innovation();