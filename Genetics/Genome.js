'use strict';

class Genome {

	constructor(inputs, outputs) {
		this.nodes = [];
		this.links = [];
		this.fitness = 0;
		this.adjustedFitness = 0;
		this.inputs = inputs;
		this.outputs = outputs;

		this.age = 0;

		for (let i = 0; i < inputs; i++) {
			let splitX = 1 / inputs * i;
			let innovation = innovationTracker.add([null, null, 'input', splitX, 0]);
			this.nodes[innovation] = {innovation: innovation, type: 'input', recurrent: false, splitX: splitX, splitY: 0};
		}

		for (let i = 0; i < outputs; i++) {
			let splitX = 1 / outputs * i;
			let innovation = innovationTracker.add([null, null, 'output',splitX, 1]);
			this.nodes[innovation] = {innovation: innovation, type: 'output', recurrent: false, splitX: splitX, splitY: 1};
		}

		let innovation = innovationTracker.add([null, null, 'bias', 0, .5]);
		this.bias = innovation;
		this.nodes[innovation] = {innovation: innovation, type: 'bias', recurrent: false, splitX: 0, splitY: 0.5};
	}

	randomWeight() {
		return Math.random() * 4 - 2;
	}

	pickRandomNeuron(nonInput, nonOutput) {
		let done = false;
		let trys = 0;
		while (!done && trys < 100) {
			trys++;
			let neuron = randomInt(0, this.nodes.length - 1);
			if (!this.nodes[neuron]) {
				continue;
			}
			if (this.nodes[neuron].type == 'bias') {
				continue;
			}
			if (nonInput && this.nodes[neuron].type == 'input') {
				continue;
			}
			if (nonOutput && this.nodes[neuron].type == 'output') {
				continue;
			}
			return neuron;
		}
		return false;
	}

	pickRandomLink() {
		return this.links[randomInt(0, this.links.length-1)];
	}

	isLinked(node1, node2) {
		for (var i = 0; i < this.links.length; i++) {
			if (this.links[i].in == node1 && this.links[i].out == node2 || this.links[i].in == node2 && this.links[i].out == node1) {
				return true;
			}
		}
		return false;
	}

	addLink(bias) {
		let node1 = false;
		let node2 = false;
		let done = false;
		let trys = 0;
		while (!done && trys < 100) {
			trys ++;
			node1 = bias ? this.bias : this.pickRandomNeuron(false, true);
			node2 = this.pickRandomNeuron(true, false);

			if (node1 === false || node2 === false) {
				return;
			}

			if (node1 == node2 || this.isLinked(node1, node2)) {
				continue;
			}

			let innovation = innovationTracker.add([node1, node2, null, null, null]);
			this.links.push({innovation: innovation, in: node1, out: node2, weight: this.randomWeight(), enabled: true});
			done = true;
		}
	}

	addNeuron() {
		let start = false;
		let end = false;
		let link;
		let sizeThreshold = this.inputs + this.outputs + 20;
		if (this.links.length < sizeThreshold) {
			let done = false;
			let trys = 0;
			while (!done && trys < 100) {
				trys++;
				link = randomInt(0, this.links.length - 1 - ~~(Math.sqrt(this.links.length)));
				if (!link || !this.links[link].enabled || this.links[link].recurrent || this.nodes[this.links[link].in].type == 'bias') {
					continue;
				}
				done = true;
				start = this.links[link].in;
				end = this.links[link].out;
			}
		} else {
			let done = false;
			let trys = 0;
			while (!done && trys < 100) {
				trys++;
				link = randomInt(0, this.links.length - 1);
				if (!link || !this.links[link].enabled || this.links[link].recurrent || this.links[link].bias) {
					continue;
				}
				done = true;
				start = this.links[link].in;
				end = this.links[link].out;
			}
		}
		if (start !== false && end !== false) {
			this.links[link].enabled = false;
			let recurrent = (this.nodes[start].splitY > this.nodes[start].splitX);
			let splitX = (this.nodes[this.links[link].in].splitX + this.nodes[this.links[link].out].splitX) / 2;
			let splitY = (this.nodes[this.links[link].in].splitY + this.nodes[this.links[link].out].splitY) / 2;
			let i1 = innovationTracker.add([null, null, 'hidden', splitX, splitY]);
			this.nodes[i1] = {innovation: i1, type: 'hidden', splitX: splitX, splitY: splitY};
			let i2 = innovationTracker.add([start, i1, null, null, null]);
			this.links.push({innovation: i2, in: start, out: i1, weight: 1, enabled: true, recurrent: recurrent});
			let i3 = innovationTracker.add([i1, end, null, null, null]);
			this.links.push({innovation: i3, in: i1 , out: end, weight: this.links[link].weight, enabled: true, recurrent: recurrent});
		} else {
			console.log('no link found');
		}

	}

	mutateWeight() {
		for (var i = 0; i < this.links.length; i++) {
			let link = this.links[i];
			if (Math.random() < constants.MUTATE_RANDOM_WEIGHT_CHANCE) {
				link.weight = this.randomWeight();
			} else {
				if (Math.random < 0.5) {
					link.weight += constants.STEPSIZE;
				} else {
					link.weight -= constants.STEPSIZE;
				}
			}
		}
	}

	enableLink() {
		let link = this.pickRandomLink();
		if (link) {
			link.enabled = true;
		}
	}

	disableLink() {
		let link = this.pickRandomLink();
		if (link) {
			link.enabled = false;
		}
	}

	mutate() {
		if (Math.random() < constants.MUTATE_BIAS_CHANCE) {
			this.addLink(true);
		}
		if (Math.random() < constants.MUTATE_LINK_CHANCE) {
			this.addLink();
		}
		if (Math.random() < constants.MUTATE_ENABLE_LINK_CHANCE) {
			this.enableLink();
		}
		if (Math.random() < constants.MUTATE_DISABLE_LINK_CHANCE) {
			this.disableLink();
		}
		if (Math.random() < constants.MUTATE_NEURON_CHANCE) {
			this.addNeuron();
		}
		if (Math.random() < constants.MUTATE_WEIGHT_CHANCE ) {
			this.mutateWeight();
		}
	}

	crossOver(genome) {
		let parent1, parent2;

		if (this.fitness > genome.fitness) {
			parent1 = this.copy();
			parent2 = genome.copy();
		} else {
			parent2 = this.copy();
			parent1 = genome.copy();	
		}

		let genes1 = [];
		let genes2 = [];

		for (let i = 0; i < parent1.nodes.length; i++) {
			if (!parent1.nodes[i]) {
				continue;
			}
			genes1[parent1.nodes[i].innovation] = ['node', parent1.nodes[i]];
		}

		for (let i = 0; i < parent1.links.length; i++) {
			genes1[parent1.links[i].innovation] = ['link', parent1.links[i]];
		}

		for (let i = 0; i < parent2.nodes.length; i++) {
			if (!parent2.nodes[i]) {
				continue;
			}
			genes2[parent2.nodes[i].innovation] = ['node', parent2.nodes[i]];
		}

		for (let i = 0; i < parent2.links.length; i++) {
			genes2[parent2.links[i].innovation] = ['link', parent2.links[i]];
		}

		let max_length = Math.max(genes1.length, genes2.length);

		let child_genes = [];
		for (let i = 0; i < max_length; i++) {
			if (!genes1[i]) {
				continue;
			}
			if (genes2[i]) {
				if (Math.random() < 0.5) {
					child_genes.push(genes1[i]);
				} else {
					child_genes.push(genes2[i]);
				}
			} else {
				child_genes.push(genes1[i]);
			}
		}

		let child = new Genome(0, 0);

		for (let i = 0; i < child_genes.length; i++) {
			if (child_genes[i][0] == 'link') {
				if (child_genes[i][1].enabled == false && Math.random() < 0.1) {
					child_genes[i][1].enabled = true;
				}
				child.links.push(child_genes[i][1]);
			} else {
				child.nodes[child_genes[i][1].innovation] = (child_genes[i][1]);
			}
		}

		child.inputs = parent1.inputs;
		child.outputs = parent1.outputs;

		return child;

	}

	copy() {
		let copy = new Genome(0, 0);
		copy.inputs = this.inputs;
		copy.outputs = this.outputs;
		copy.age = this.age;
		copy.fitness = this.fitness;
		copy.nodes = JSON.parse(JSON.stringify(this.nodes));
		copy.links = JSON.parse(JSON.stringify(this.links));
		return copy;
	}

	disjoint(genome) {
		let disjoint = 0;
		let innovations1 = [];
		let innovations2 = [];

		for (let i = 0; i < this.nodes.length; i++) {
			if (!this.nodes[i]) {
				continue;
			}
			innovations1.push(this.nodes[i].innovation);
		}

		for (let i = 0; i < this.links.length; i++) {
			innovations1.push(this.links[i].innovation);
		}

		for (let i = 0; i < genome.nodes.length; i++) {
			if (!genome.nodes[i]) {
				continue;
			}
			innovations2.push(genome.nodes[i].innovation);
		}

		for (let i = 0; i < genome.links.length; i++) {
			innovations2.push(genome.links[i].innovation);
		}


		for (let i = 0; i < innovations1.length; i++) {
			if (innovations2.indexOf(innovations1[i]) == -1) {
				disjoint++;
			}
		}

		for (let i = 0; i < innovations2.length; i++) {
			if (innovations1.indexOf(innovations2[i]) == -1) {
				disjoint++;
			}
		}

		return disjoint;
	}

	weightDifference(genome) {
		let innovations1 = [];
		let innovations2 = [];
		let compare = [];
		for (let i = 0; i < this.links.length; i++) {
			innovations1[this.links[i].innovation] = this.links[i];
		}

		for (let i = 0; i < genome.links.length; i++) {
			innovations2[genome.links[i].innovation] = genome.links[i];
		}

		let max = Math.max(innovations1.length, innovations2.length);

		for (let i = 0; i < max; i++) {
			if (innovations1[i] && innovations2[i]) {
				compare.push([innovations1[i].weight, innovations2[i].weight]);
			}
		}

		let difference = 0;

		for (let i = 0; i < compare.length; i++) {
			difference += Math.abs(compare[i][0] - compare[i][1]);
		}

		difference /= compare.length > 0 ? compare.length : 1;

		return difference;
	}

	difference(genome) {
		return this.disjoint(genome) + (this.weightDifference(genome) / 2); 
	}
}