'use strict';

class Pool {

	constructor() {
		this.current = 0;
		this.species = [];
		this.global = [];
		this.averageFitness = 0;
		this.topFitnes = 0;
		this.species_count = 0;
		this.genome_count = 0;
		this.underTest = null;
		this.best = null;
	}

	setCurrentFitness(score) {
		let adjusted_score = score;
		if (this.underTest.age < 10) {
			adjusted_score *= 1.3;
		}

		if (this.underTest.age > 50) {
			adjusted_score *= 0.7;
		}
		this.underTest.adjustedFitness = adjusted_score;
		this.underTest.fitness = score;
		this.underTest.last = score;
	}

	addGenome(genome) {
		let added = false;
		for (let i = 0; i < this.species.length; i++) {
			if(this.species[i].ispartOf(genome)) {
				this.species[i].addGenome(genome);
				added = true;
				break;
			}
		}
		if (!added) {
			let species = new Species();
			species.addGenome(genome);
			this.species.push(species);
		}

	}

	nextCandidate() {
		if (!this.global[this.current]) {
			this.generation();
			this.current = 0;
		}
		if (window.playBest) {
			this.underTest = this.best;
		} else {		
			this.underTest = this.global[this.current];
			this.current++;
		}
		return this.underTest;
	}

	// pick(sMax) {
	// 	let s = randomInt(0, ~~sMax);
	// 	for (var i = 0; i < this.species.length; i++) {
	// 		s -= this.species[i].averageFitness;
	// 		if (s <= 0) {
	// 			return this.species[i];
	// 		}
	// 	}
	// 	return this.species[this.species.length - 1];
	// }

	generation() {
		// remove bottom half of each species
		for (let i = 0; i < this.species.length; i++) {
			this.species[i].sort(true);
			this.species[i].genomes = this.species[i].genomes.splice(0, Math.ceil(this.species[i].genomes.length / 2));
			this.species[i].sort(false);
		}


		// prepare species
		for (let i = 0; i < this.species.length; i++) {
			this.species[i].age++;
			this.species[i].claculateAverageFitness();
			this.species[i].claculateTopFitness();
		}

		this.averageFitness = 0;
		this.topFitnes = 0;

		for (let i = 0; i < this.species.length; i++) {
			this.averageFitness += this.species[i].averageFitness;
			if (this.species[i].topFitnes > this.topFitnes) {
				this.topFitnes = this.species[i].topFitnes;
				if (!this.best || this.species[i].topFitnes > this.best.fitness) {
					this.best = this.species[i].genomes[0].copy();
				}
			}
		}

		this.averageFitness /= this.species.length;

		let survived = [];

		// kill weak old species
		for (let i = 0; i < this.species.length; i++) {
			if (this.species[i].age > 5 && this.species[i].averageFitness < this.averageFitness / 2) {
				// keep best genome from species if its better than average
				if (this.species[i].topFitnes > this.averageFitness) {
					let species = new Species();
					species.addGenome(this.species[i].genomes[0]);
					species.claculateTopFitness();
					species.claculateAverageFitness();
					survived.push(species);
					console.log('keeping only best from species ' + i);
				} else {
					console.log('removing species ' + i + ' fitness: ' + this.species[i].averageFitness);
				}
			} else {
				survived.push(this.species[i]);
			}
		}
		this.species = survived;

		// order species by average fitness
		this.species.sort(function(a, b) {
			return b.averageFitness - a.averageFitness;
		});

		// calculate average fitness of all genomes in the pool
		let averageFitnessSum = 0;
		let new_length = 0;

		for (let i = 0; i < this.species.length; i++) {
			averageFitnessSum += this.species[i].averageFitness;
			new_length += this.species[i].genomes.length;
		}

		// copy top genome of each species to next generation
		let elite = [];

		for (var i = 0; i < this.species.length; i++) {
			new_length++;
			elite.push(this.species[i].genomes[0].copy());
		}

		// spawn new childs
		let spawn_total = constants.MAX_POPULATION - new_length;

		for (let s = 0; s < this.species.length; s++) {
			let spawn_species = Math.round((this.species[s].averageFitness / averageFitnessSum) * spawn_total);
			for (var c = 0; c < spawn_species; c++) {
				this.addGenome(this.species[s].breedChild());
			}
		}

		this.global = [];

		for (let s = 0; s < this.species.length; s++) {
			for (var g = 0; g < this.species[s].genomes.length; g++) {
				for (var i = 0; i < constants.MUTATION_RATE; i++) {
					this.species[s].genomes[g].mutate();
				}
				this.species[s].genomes[g].age++;
				this.global.push(this.species[s].genomes[g]);
			}	
		}

		// add "elite" without mutation
		for (let i = 0; i < elite.length; i++) {
			this.addGenome(elite[i]);
			this.global.push(elite[i]);
		}

		// only used for display
		this.species_count = this.species.length;
		this.genome_count = this.global.length;
	}
}