'use strict';

function drawCircle(ctx, centerX, centerY) {
	let radius = 10;
	ctx.beginPath();
	ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
	ctx.fill();
	ctx.lineWidth = 1;
}

function randomInt(min, max) {
	return ~~(Math.random() * (max - min + 1)) + min;
}

function sum(n) {
	let sum = 0;
	for (var i = 1; i <= n; i++) {
		sum += i;
	}
	return sum;
}