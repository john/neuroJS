'use strict';


class Network {
	constructor(genome) {

		this.genome = genome;

		this.neurons = [];
		this.inputs = [];
		this.outputs = [];

		for (let i = 0; i < genome.nodes.length; i++) {
			if (!genome.nodes[i]) {
				continue;
			}
			let neuron = {value: 0, lastValue: 0, outputs: [], splitX: genome.nodes[i].splitX, splitY: genome.nodes[i].splitY, type: genome.nodes[i].type};
			this.neurons[genome.nodes[i].innovation] = neuron;
			if (genome.nodes[i].type == 'input') {
				this.inputs.push(neuron);
			}
			if (genome.nodes[i].type == 'output') {
				this.outputs.push(neuron);
			}
		}

		for (let i = 0; i < genome.links.length; i++) {
			if (genome.links[i].enabled) {
				this.neurons[genome.links[i].in].outputs.push([genome.links[i].out, genome.links[i].weight]);
			}
		}
	}


	draw() {
		let canvas = document.getElementById('network');
		let ctx = canvas.getContext('2d');
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		for (let i = 0; i < this.neurons.length; i++) {
			ctx.beginPath();
			if (!this.neurons[i]) {
				continue;
			}
			if (this.neurons[i].type == 'output') {
				ctx.fillStyle = this.neurons[i].lastValue > 0 ? 'green' : 'white';
			} else if(this.neurons[i].type == 'input') {
				if (i < constants.INPUTS / 3) {
					ctx.fillStyle = this.neurons[i].lastValue > 0 ? 'red' : 'white';
				} else if(i < constants.INPUTS / 1.5) {
					ctx.fillStyle = this.neurons[i].lastValue > 0 ? 'black' : 'white';
				} else {
					ctx.fillStyle = this.neurons[i].lastValue > 0 ? 'yellow' : 'white';
				}
			} else {
				ctx.fillStyle = this.neurons[i].lastValue > 0 ? 'blue' : 'white';
			}
			ctx.strokeStyle = '#003300';
			let centerX = (canvas.width - 100) * this.neurons[i].splitX + 50;
			let centerY = (canvas.height - 100) * this.neurons[i].splitY + 50;
			drawCircle(ctx, centerX, centerY);
			ctx.stroke();
			ctx.closePath();
		}
		for (let n = 0; n < this.neurons.length; n++) {
			if (!this.neurons[n]) {
				continue;
			}
			for (let l = 0; l < this.neurons[n].outputs.length; l++) {
				ctx.beginPath();
				let input = this.neurons[n];
				let output = this.neurons[input.outputs[l][0]];
				let blue = Math.max(Math.min((((input.lastValue * this.neurons[n].outputs[l][1]) / 2) * 255), 255), 0);
				let red = Math.max(Math.min(((((input.lastValue * -1) * this.neurons[n].outputs[l][1]) / 2) * 255) , 255), 0);
				let color = 'rgb(' + ~~red + ',0,' + ~~blue + ')';
				ctx.strokeStyle = color;
				//console.log(color)
				//console.log(input.value)
				ctx.lineWidth = 2;
				ctx.moveTo((canvas.width - 100) * input.splitX + 50, (canvas.height - 100) * input.splitY + 50 + 10);
				ctx.lineTo((canvas.width - 100) * output.splitX + 50, (canvas.height - 100) * output.splitY + 50 - 10);
				ctx.stroke();
				ctx.closePath();
			}
		}
	}

	getOutputs() {
		var out = [];
		for (let i = 0; i < this.outputs.length; i++) {
			out.push(this.outputs[i].value > 0);
		}
		return out;
	}

	fire() {
		for (var neuron = 0; neuron < this.neurons.length; neuron++) {
			if (!this.neurons[neuron]) {
				continue;
			}
			let value = this.activate(this.neurons[neuron].value);
			if (this.neurons[neuron].type !== 'input') {
				value = this.activate(this.neurons[neuron].value);
			}
			if (this.neurons[neuron].type == 'bias') {
				value = 1;
			}
			this.neurons[neuron].lastValue = value;
			for (var output = 0; output < this.neurons[neuron].outputs.length; output++) {
				let target = this.neurons[this.neurons[neuron].outputs[output][0]];
				target.value += value * this.neurons[neuron].outputs[output][1];
			}
		}
	}

	activate(x) {
		return 2/(1+Math.exp(-4.9*x))-1;
	}
}