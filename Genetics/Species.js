'use strict';

class Species {
	constructor() {
		this.genomes = [];
		this.averageFitness = 0;
		this.topFitnes = 0;
		this.age = 0;
	}

	sort(adjusted) {
		if (adjusted) {
			this.genomes.sort(function(a, b) {
				return b.adjustedFitness - a.adjustedFitness;
			});
		} else {
			this.genomes.sort(function(a, b) {
				return b.fitness - a.fitness;
			});
		}
	}

	addGenome(genome) {
		this.genomes.push(genome);
	}

	ispartOf(genome) {
		let average = 0;
		for (var i = 0; i < this.genomes.length; i++) {
			average += this.genomes[i].difference(genome);
		}
		average /= this.genomes.length;
		if (average < constants.SAME_SPECIES) {
			return true;
		} else {
			return false;
		}
	}

	breedChild() {
		if (this.genomes.length == 1 || Math.random() > constants.CROSSOVER_CHANCE) {
			return this.pick().copy();
		} else {
			let parent1 = this.pick();
			let parent2 = this.pick();

			return parent1.crossOver(parent2);
		}
	}

	pick() {
		let s = randomInt(0, sum(this.genomes.length));
		for (var i = 0; i < this.genomes.length; i++) {
			s -= this.genomes.length - i;
			if (s <= 0) {
				return this.genomes[i];
			}
		}
		return this.genomes[this.genomes.length - 1];
	}

	claculateAverageFitness() {
		let average = 0;
		for (var i = 0; i < this.genomes.length; i++) {
			average += this.genomes[i].fitness;
		}
		this.averageFitness = average / this.genomes.length;

	}

	claculateTopFitness() {
		this.topFitnes = 0;
		for (var i = 0; i < this.genomes.length; i++) {
			if (this.genomes[i].fitness > this.topFitnes) {
				this.topFitnes = this.genomes[i].fitness;
			}
		}
	}
}